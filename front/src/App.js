import React from 'react';
import {Container, CssBaseline} from "@material-ui/core";
import {Route, Switch} from 'react-router-dom';
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import AllProducts from "./containers/AllProducts/AllProducts";


const App = () => (
    <>
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={AllProducts} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
          </Switch>
        </Container>
      </main>
    </>
);

export default App;
