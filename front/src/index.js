import React from "react";
import ReactDOM from "react-dom";
import {Router} from 'react-router-dom';
import App from "./App";
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {Provider} from "react-redux";
import thunkMiddleware from 'redux-thunk';
import {NotificationContainer} from 'react-notifications';
import usersReducer from "./store/reducer/usersReducer";
import history from "./history";

const rootReducer = combineReducers({
    users: usersReducer,

});

const saveToLocalStorage = state => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('statePost', serializedState);
    } catch (e) {
        console.log('Could not save state');
    }
};

const loadFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('statePost');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (e) {
        return undefined;
    }
};


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();


const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
    saveToLocalStorage({
        users: store.getState().users
    });
});


const theme = createMuiTheme({
    props: {
        MuiTextField: {
            variant: 'outlined',
            fullWidth: true,
        }
    }
});
const app = (
    <Provider store={store}>
        <Router history={history}>
            <MuiThemeProvider theme={theme}>
                <NotificationContainer/>
                <App/>
            </MuiThemeProvider>
        </Router>
    </Provider>
);
ReactDOM.render(app, document.getElementById("root"));