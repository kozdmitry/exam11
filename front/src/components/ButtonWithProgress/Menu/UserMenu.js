import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {Button, Menu, MenuItem} from "@material-ui/core";
import {Link} from "react-router-dom";
import {logoutUser} from "../../../store/actions/usersActions";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <>
            <Button onClick={handleClick} color="inherit">
                Hello {user.username}!
            </Button>
            <Menu anchorEl={anchorEl}
                  onClose={handleClose}
                  open={Boolean(anchorEl)}
            >
                <MenuItem component={Link} to="/new">
                    Add POST!
                </MenuItem>
                <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
            </Menu>

        </>
    );
};

export default UserMenu;