const mongoose = require('mongoose');
const config = require("./config");
const User = require("./models/User");
const Item = require("./models/Item");
const Category = require("./models/Category");
const {nanoid} = require("nanoid");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (let coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    };

    const [AutoCategory, MobilePhoneCategory, OtherCategory] = await Category.create({
        title: 'Auto',
        description: 'All for a car'
    }, {
        title: 'Mobile',
        description: 'All for a mobile phone'
    }, {
        title: 'Other',
        description: 'Unnecessary things'
    });

    await Item.create({
        title: 'Denso oxygen sensor for Toyota',
        description: 'Ordered this sensor and 234-4233 as direct replacements for my 2003 Toyota Corolla LE.',
        category: AutoCategory,
        image: 'fixtures/OxygenToyota.jpg',
    },{
        title: 'New BMWX5M',
        description: 'Parts for BMW X5M',
        category: AutoCategory,
        image: 'fixtures/X5M.jpg',
    }, {
        title: 'Xiaomi mi8Lite',
        description: "Parts for Xiaomi mobilePhone",
        category: MobilePhoneCategory,
        image: 'fixtures/xiaomiparts.jpg',
    }, {
        title: 'Iphone 6 Plus',
        description: "Prodayu iphone 6 plus ideal sost",
        category: MobilePhoneCategory,
        image: 'fixtures/iphone6.jpg',
    }, {
        title: 'Furniture for sale',
        description: "Table, chair, sofa, armchair",
        category: OtherCategory,
        image: 'fixtures/sofa.jpg',
    });

    await User.create({
        username: 'user',
        password: '1qaz@WSX29',
        token: nanoid()
    }, {
        username: 'admin',
        password: '1qaz@WSX29',
        token: nanoid()
    });

    await mongoose.connection.close();
};

run().catch(console.error);